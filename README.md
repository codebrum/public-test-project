Data Persistence in Unity
(Data Persistence Between Scenes & Sessions - JSON)


Player name (saving data between scenes)

- Create a new Start Menu scene for the game with a text entry field prompting the user to enter their name, and a Start button.
- When the user clicks the Start button, the Main game scene will be loaded and their name will be displayed next to the score. 

High score (saving data between sessions):

- As the user plays, the current high score will be displayed on the screen alongside the player name who created the score.
- If the high score is beaten, the new score and player name will be displayed instead.
- The highest score will be saved between sessions, so that if the player closes and reopens the application, the high score and player name will be retained.