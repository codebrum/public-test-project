using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

// Sets the script to be executed later than all default scripts
// This is helpful for UI, since other things may need to be initialized before setting the UI
[DefaultExecutionOrder(1000)]
public class MenuUIHandler : MonoBehaviour
{
    private TMP_InputField _inputField;
    private TextMeshProUGUI _bestScoreText;
    private Button _startBtn;

    private void Awake()
    {
        _bestScoreText = transform.GetChild(0).Find("BestScoreText").GetComponent<TextMeshProUGUI>();
        _inputField = transform.GetChild(0).Find("NameInputField").GetComponent<TMP_InputField>();
        _startBtn = transform.GetChild(0).Find("StartBtn").GetComponent<Button>();
    }

    private void Start()
    {
        _startBtn.onClick.AddListener(StartNewPlayer);

        ShowBestScoreBoard();
    }

    private void ShowBestScoreBoard()
    {
        _bestScoreText.text = SaveDataManager.ShowBestScorer(out SaveDataManager.LeaderBoardEntryData data)
            ? $"Best Score: {data.name} : {data.score}"
            : $"Best Score: {SaveDataManager.GetActivePlayerName()} : {0}";
    }

    public void StartNewPlayer()
    {
        SaveDataManager.SavePlayerProfile(_inputField.text);
        SaveDataManager.SetActivePlayer(_inputField.text);

        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }
}