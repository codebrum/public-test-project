using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class SaveDataManager
{
    [Serializable]
    private class PlayerProfilesData
    {
        public List<string> playerNames;

        public PlayerProfilesData(List<string> names)
        {
            playerNames = names;
        }
    }

    [Serializable]
    public class LeaderBoardEntryData
    {
        public string name;
        public int score;

        public LeaderBoardEntryData(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }

    [Serializable]
    private class LeaderBoardListData
    {
        public List<LeaderBoardEntryData> entries;

        public LeaderBoardListData(List<LeaderBoardEntryData> entries)
        {
            this.entries = entries;
        }
    }

    private static string GetSavePlayersFilePath()
    {
        return Application.persistentDataPath + "/players.json";
    }

    public static void SavePlayerProfile(string playerName)
    {
        if (String.IsNullOrWhiteSpace(playerName))
        {
            playerName = GetActivePlayerName();
        }

        GetSavedPlayerProfiles(out List<string> players);

        if (players.Contains(playerName))
            return;

        players.Insert(0, playerName);
        SavePlayerProfilesFromList(players);
    }

    private static bool GetSavedPlayerProfiles(out List<string> data)
    {
        if (File.Exists(GetSavePlayersFilePath()))
        {
            string dataJson = File.ReadAllText(GetSavePlayersFilePath());
            PlayerProfilesData loadedData = JsonUtility.FromJson<PlayerProfilesData>(dataJson);
            data = loadedData.playerNames;
            return true;
        }

        data = new List<string>();
        return false;
    }

    private static void SavePlayerProfilesFromList(List<string> players)
    {
        PlayerProfilesData data = new PlayerProfilesData(players);
        string dataJson = JsonUtility.ToJson(data, true);
        File.WriteAllText(GetSavePlayersFilePath(), dataJson);
    }

    public static void DeletePlayerProfile(string playerName)
    {
        GetSavedPlayerProfiles(out List<string> players);
        players.Remove(playerName);
        SavePlayerProfilesFromList(players);
    }

    public static void SaveNewLeaderBoardEntry(string name, int score)
    {
        LeaderBoardEntryData newEntry = new LeaderBoardEntryData(name, score);
        GetSavedLeaderBoardEntryList(out List<LeaderBoardEntryData> entries);
        if (entries.Count == 0)
        {
            entries.Add(newEntry);
        }
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (newEntry.score > entries[i].score)
                {
                    entries.Insert(i, newEntry);
                    break;
                }
            }
        }

        LeaderBoardListData data = new LeaderBoardListData(entries);
        string dataJson = JsonUtility.ToJson(data, true);
        File.WriteAllText(GetLeaderBoardSaveDir(), dataJson);
    }

    public static bool GetSavedLeaderBoardEntryList(out List<LeaderBoardEntryData> entries)
    {
        if (File.Exists(GetLeaderBoardSaveDir()))
        {
            string loadedDataJson = File.ReadAllText(GetLeaderBoardSaveDir());
            LeaderBoardListData loadedData = JsonUtility.FromJson<LeaderBoardListData>(loadedDataJson);
            entries = loadedData.entries;
            return true;
        }

        entries = new List<LeaderBoardEntryData>();
        return false;
    }

    private static string GetLeaderBoardSaveDir()
    {
        return Application.persistentDataPath + "/" + "LeaderBoard.json";
    }

    public static void SetActivePlayer(string playerName)
    {
        GetSavedPlayerProfiles(out List<string> players);
        if (players.Remove(playerName))
        {
            players.Insert(0, playerName);
            SavePlayerProfilesFromList(players);
        }
    }

    public static string GetActivePlayerName()
    {
        GetSavedPlayerProfiles(out List<string> players);
        if (players.Count != 0)
        {
            return players[0];
        }

        return "unknown";
    }

    public static LeaderBoardEntryData GetTopScorer()
    {
        GetSavedLeaderBoardEntryList(out List<LeaderBoardEntryData> entries);
        if (entries != null)
        {
            var result = entries.OrderByDescending(e => e.score);
            return result.FirstOrDefault();
        }

        return null;
    }

    public static bool ShowBestScorer(out LeaderBoardEntryData entry)
    {
        if (GetSavedLeaderBoardEntryList(out List<LeaderBoardEntryData> entries))
        {
            entry = GetTopScorer();
            return true;
        }

        entry = new LeaderBoardEntryData("unknown", 0);
        return false;
    }
}