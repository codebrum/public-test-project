using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance;
    // private List<ScoreBoard> _scoreBoard;

    // public List<ScoreBoard> ScoreList => _scoreBoard;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        // _scoreBoard = new List<ScoreBoard>();
    }

    public void SaveGameData(string newName)
    {
        // var scoreBoard = new ScoreBoard { name = newName, score = 0 };

        // _scoreBoard.Add(scoreBoard);
    }
    
    public void LoadGameData()
    {
        
    }
}